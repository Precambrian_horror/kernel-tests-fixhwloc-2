# storage/nvme_rdma/hardtarget/nvmeof_rdma_fio_stress

Storage: nvmeof rdma discover stress testing with NETAPP E5700

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
